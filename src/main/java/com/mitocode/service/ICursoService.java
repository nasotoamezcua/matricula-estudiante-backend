package com.mitocode.service;

import com.mitocode.model.Curso;

public interface ICursoService extends IGenericService<Curso, String> {

}
