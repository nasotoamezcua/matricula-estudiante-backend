package com.mitocode.model;

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Datos para el modelo Matricula")
@Document(collection = "matriculas")
public class Matricula {
	
	@Id
	private String id;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@NotNull
	private LocalDate fechaMatricula;
	
	@ApiModelProperty(value = "El objeto estudiante no puede ser nulo")
	@NotNull
	private Estudiante estudiante;
	
	@ApiModelProperty(value = "El arreglo cursos no puede ser nulo")
	@NotNull
	private List<Curso> cursos;
	
	@ApiModelProperty(value = "La propiedad estado no puede ser nulo")
	@Field
	@NotNull
	private Boolean estado;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDate getFechaMatricula() {
		return fechaMatricula;
	}

	public void setFechaMatricula(LocalDate fechaMatricula) {
		this.fechaMatricula = fechaMatricula;
	}

	public Estudiante getEstudiante() {
		return estudiante;
	}

	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
}
