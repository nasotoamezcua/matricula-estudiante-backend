package com.mitocode.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Datos para el modelo Estudiante")
@Document(collection = "estudiantes")
public class Estudiante {
	
	@Id
	private String id;
	
	@ApiModelProperty(value = "La propiedad nombres no puede ser nulo")
	@Field
	@NotEmpty
	private String nombres;
	
	@ApiModelProperty(value = "La propiedad apellidos no puede ser nulo")
	@Field
	@NotEmpty
	private String apellidos;
	
	@ApiModelProperty(value = "La propiedad dni no puede ser nulo")
	@Field
	@NotEmpty
	private String dni;
	
	@ApiModelProperty(value = "La propiedad edad no puede ser nulo")
	@Field
	@NotNull
	private Integer edad;
	
	public Estudiante() {}

	public Estudiante(String id, @NotEmpty String nombres, @NotEmpty String apellidos, @NotEmpty String dni,
			@NotEmpty Integer edad) {
		this.id = id;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.dni = dni;
		this.edad = edad;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

}
