package com.mitocode.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Datos para el modelo Curso")
@Document(collection = "cursos")
public class Curso {
	
	@Id
	private String id;
	
	@ApiModelProperty(value = "La propiedad nombre no puede ser nulo")
	@Field
	@NotEmpty
	private String nombre;
	
	@Field
	private String siglas;
	
	@ApiModelProperty(value = "La propiedad estado no puede ser nulo")
	@Field
	@NotNull
	private Boolean estado;
	
	public Curso() {}
	

	public Curso(String id, @NotEmpty String nombre, String siglas, @NotNull Boolean estado) {
		this.id = id;
		this.nombre = nombre;
		this.siglas = siglas;
		this.estado = estado;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSiglas() {
		return siglas;
	}

	public void setSiglas(String siglas) {
		this.siglas = siglas;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
}
