package com.mitocode.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Estudiante;
import com.mitocode.service.IEstudianteService;
import com.mitocode.validators.RequestValidator;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/estudiantes")
public class EstudianteRestController {
	
	@Autowired
	private IEstudianteService service;
	
	@Autowired
	private RequestValidator validadorGeneral;
	
	@GetMapping
	public Mono<ResponseEntity<Flux<Estudiante>>> listar(){
		
		Flux<Estudiante> fxCursos = service.listar();
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(fxCursos));
	}
	
	@GetMapping("/orderByEdad")
	public Mono<ResponseEntity<Flux<Estudiante>>> listarEstudiantesPorEdad(){
		
		Flux<Estudiante> fxCursos = service.listarEstudiantesPorEdad();
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(fxCursos));
	}
	
	@GetMapping("{id}")
	public Mono<ResponseEntity<Estudiante>> listarPorId(@PathVariable("id") String id){
		return service.listarPorId(id)
				.map(c -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(c))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@PostMapping
	public Mono<ResponseEntity<Estudiante>> registrar(@Valid @RequestBody Estudiante estudiante, final ServerHttpRequest req){
		
		Mono<Estudiante> monoCurso = Mono.just(estudiante);
		
		return monoCurso
				.flatMap(validadorGeneral::validate)
				.flatMap(service::registrar)
				.map(c -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(c.getId())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(c));
	}
	
	@PutMapping
	public Mono<ResponseEntity<Estudiante>> modificar(@Valid @RequestBody Estudiante estudiante){
		
		Mono<Estudiante> monoCurso = Mono.just(estudiante);
	
		return monoCurso
				.flatMap(validadorGeneral::validate)
				.flatMap(service::registrar)
				.map(c -> ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(c));
		
	}
	
	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id){
		
		return service.listarPorId(id) 
		.flatMap( c -> {
			return service.eliminar(c.getId()) //Mono<Void>
					.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
		})
		.defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
	}
	

}
