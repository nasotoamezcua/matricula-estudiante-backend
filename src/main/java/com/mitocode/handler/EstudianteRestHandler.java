package com.mitocode.handler;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.mitocode.model.Estudiante;
import com.mitocode.service.IEstudianteService;
import com.mitocode.validators.RequestValidator;

import reactor.core.publisher.Mono;

@Component
public class EstudianteRestHandler {
	
	@Autowired
	private IEstudianteService service;
	
	@Autowired
	private RequestValidator validadorGeneral;
	
	public Mono<ServerResponse> listar(ServerRequest req){
		return ServerResponse
				.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(service.listar(), Estudiante.class);
	}
	
	public Mono<ServerResponse> listarEstudiantesPorEdad(ServerRequest req){
		return ServerResponse
				.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(service.listarEstudiantesPorEdad(), Estudiante.class);
	}
	
	public Mono<ServerResponse> listarPorId(ServerRequest req){		
		
		String id = req.pathVariable("id");
		return service.listarPorId(id)
				.flatMap(c -> ServerResponse
						.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(fromValue(c))
				)
				.switchIfEmpty(ServerResponse.notFound().build());
	}
	
	public Mono<ServerResponse> registrar(ServerRequest req){
		
		Mono<Estudiante> monoEstudiante =  req.bodyToMono(Estudiante.class);		
		return monoEstudiante
				.flatMap(validadorGeneral::validate)
				.flatMap(service::registrar)
				.flatMap(c -> ServerResponse.created(URI.create(req.uri().toString().concat("/").concat(c.getId())))
				.contentType(MediaType.APPLICATION_JSON)
				.body(fromValue(c)));
	}
	
	public Mono<ServerResponse> modificar(ServerRequest req){
		
		Mono<Estudiante> monoEstudiante =  req.bodyToMono(Estudiante.class);		
		return monoEstudiante
				.flatMap(validadorGeneral::validate)
				.flatMap(service::registrar)
				.flatMap(c -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(fromValue(c)));
	}
	
	public Mono<ServerResponse> eliminar(ServerRequest req){		
		
		String id = req.pathVariable("id");				
		return service.listarPorId(id)
				.flatMap(c -> service.eliminar(c.getId())
						.then(ServerResponse.noContent().build())						
				)
				.switchIfEmpty(ServerResponse.notFound().build());
	}
	
	

}
