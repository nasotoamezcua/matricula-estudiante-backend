package com.mitocode.repo;

import org.springframework.stereotype.Repository;

import com.mitocode.model.Estudiante;

@Repository
public interface IEstudianteRepo extends IGenericRepo<Estudiante, String> {

}
