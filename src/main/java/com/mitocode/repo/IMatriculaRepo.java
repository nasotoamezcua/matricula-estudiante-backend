package com.mitocode.repo;

import org.springframework.stereotype.Repository;

import com.mitocode.model.Matricula;

@Repository
public interface IMatriculaRepo extends IGenericRepo<Matricula, String> {

}
