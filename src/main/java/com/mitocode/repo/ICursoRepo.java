package com.mitocode.repo;

import org.springframework.stereotype.Repository;

import com.mitocode.model.Curso;

@Repository
public interface ICursoRepo extends IGenericRepo<Curso, String> {

}
