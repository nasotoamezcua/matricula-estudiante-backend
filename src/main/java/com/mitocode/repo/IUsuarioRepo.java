package com.mitocode.repo;

import org.springframework.stereotype.Repository;

import com.mitocode.model.Usuario;

import reactor.core.publisher.Mono;

@Repository
public interface IUsuarioRepo extends IGenericRepo<Usuario, String> {
	
	Mono<Usuario> findOneByUsuario(String usuario);

}
